export default {
  format: 'cjs',
  external: ['buffer', 'chalk', 'chokidar', 'debug', 'events', 'fs-extra', 'glob', 'minimatch', 'os', 'path', 'rollup', 'source-map-support', 'stream', 'thenify', 'yargs']
}
