/**
 *
 * gaemon
 * watcher
 *
 */


import { watch as chokidar } from 'chokidar';
import { EventEmitter } from 'events';
import { join as joinPaths, relative as getPathRelation } from 'path';
import newDebug from 'debug';


export default class Watcher extends EventEmitter {
  constructor(relativePath, { changeDelay = 500, root }) {
    super();

    let path = joinPaths(root, relativePath);

    this.debug = newDebug('gaemon:watcher');
    this.fswatcher = chokidar(path);
    this.options = { changeDelay };

    this.timeout = null;
    this.running = false;
    this.waitingFiles = new Set();

    // TODO: add support for 'unlink' (remove) events @watcher
    let handler = (path) => {
      let relativePath = getPathRelation(root, path);

      this.change(relativePath);
      this.emit('change', { relativePath });
    };

    this.fswatcher
      .on('add', handler)
      .on('change', handler);
  }

  change(relativePath) {
    this.debug('change (path: %s)', relativePath);

    if (this.timeout !== null) {
      this.debug('wait reset');
      clearTimeout(this.timeout);
    }

    this.waitingFiles.add(relativePath);

    this.timeout = setTimeout(() => {
      if (!this.running) {
        this.debug('run ready (type: default)');
        this.emit('run:ready', { files: Array.from(this.waitingFiles) });
      } else {
        this.debug('wait');
      }
    }, this.options.changeDelay);
  }

  run(deferrer) {
    if (this.running) {
      throw new Error('Already running');
    }

    this.running = true;
    this.timeout = null;

    this.debug('run (%s files)', this.waitingFiles.size);
    this.emit('run');

    this.waitingFiles.clear();

    return deferrer
      .then(() => {
        this.debug('run done');
        this.emit('run:done');
      })
      .catch((err) => {
        this.debug('run error (message: %s)', err.message);
        this.emit('run:error', err);
      })
      .then(() => {
        this.reset();
      });
  }

  reset() {
    if (!this.running) {
      throw new Error('Not running');
    }

    this.running = false;

    if (this.waitingFiles.size > 0) {
      this.debug('run ready (type: waited)');
      this.emit('run:ready', { files: Array.from(this.waitingFiles) });
    }
  }

  stop() {
    this.debug('stop');
    this.fswatcher.close();
    this.removeAllListeners();

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }
}
