/**
 *
 * gaemon
 * CLI API
 *
 */


import { join as joinPaths } from 'path';

import Controller from './controller';


export function clean({ root }) {
  let controller = new Controller(root);

  return controller.clean();
}

export function run(taskName, { confFilePath, enableModuleBundler, enableSourceMaps, root, watch }) {
  let controller;

  return Promise.resolve()
    .then(() => {
      return confFilePath && confFilePath !== 'auto'
        ? confFilePath
        : Controller.findConfiguration({ root });
    })
    .then((confFileRelativePath) => {
      controller = new Controller({ conf: { enableModuleBundler, enableSourceMaps, relativePath: confFileRelativePath }, root });

      return controller.loadConfiguration();
    })
    .then(() => {
      return controller.findTask(taskName);
    })
    .then((task) => {
      return controller.runTask(task).then(() => task);
    })
    .then((task) => {
      if (watch) {
        return controller.watchPipeline(task);
      }
    });
}
