/**
 *
 * gaemon
 * vinyl filesystem
 *
 */


import { Buffer } from 'buffer';
import { basename, dirname, join as joinPaths, relative as getPathRelation } from 'path';
import { Readable as ReadableStream, Writable as WritableStream } from 'stream';

import { F_OK, createReadStream, emptyDir, ensureSymlink as symlink, outputFile as writeFile, stat } from './mz/fs';


function getRandomHexadecimal() {
  return Math.floor(Math.random() * Math.pow(10, 18)).toString(16);
}


// Equivalent to 644
const defaultFileMode = 420;

/**
 * An object representing a file set.
 */
export class FileSet {
  /**
   * Creates a new virtual file set from a controller.
   */
  constructor({ controller, files = [] }) {
    this.controller = controller;
    this.files = files;
    this.symlinks = [];
  }

  /**
   * Returns a cloned file.
   */
  clone() {
    return new FileSet({
      controller: this.controller,
      files: this.files.map((f) => f.clone())
    });
  }

  /**
   * Returns the same file set but emptied (keeping the controller).
   */
  empty({ files } = {}) {
    return new FileSet({
      controller: this.controller,
      files
    });
  }

  /**
   * Returns true if a file which path is 'relativePath' exists.
   */
  exists(relativePath) {
    return this.files.some((file) => {
      return file.relativePath === relativePath;
    });
  }

  findFile(relativePath) {
    for (let file of this.files) {
      if (file.relativePath === relativePath) {
        return file;
      }
    }

    return null;
  }

  /**
   * Returns the unique file contained, if any, or null.
   */
  get mainFile() {
    return this.files.length === 1
      ? this.files[0]
      : null;
  }

  /**
   * Merges the fileset with others.
   * Chainable.
   */
  concat(fset0, ...fsets) {
    this.files = this.files.concat(fset0.files);

    if (fsets.length > 0) {
      this.concat(...fsets);
    }

    this.removeDuplicates();

    return this;
  }


  removeDuplicates() {
    let known = [];

    this.files = this.files.filter((file) => {
      let path = file.generatePath(this.root);

      if (known.indexOf(path) >= 0) {
        return false;
      }

      known.push(path);
      return true;
    });
  }

  /**
   * Returns the file set's controller's root path.
   */
  get root() {
    return this.controller.root;
  }

  /**
   * Writes the file set's files to the controller's temporary directory.
   */
  save() {
    let tmpId = getRandomHexadecimal();
    let tmpDir = `${this.controller.tmpDirs.filesets}/${tmpId}`;

    // also makes sure directory is created
    return emptyDir(tmpDir)
      .then(() => {
        return Promise.all(
          this.files.map((file) => file.writeToFS(tmpDir))
        );
      })
      .then(() => {
        return Promise.all(
          this.symlinks.map((fullLocation) => {
            let srcPath = joinPaths(this.root, fullLocation);
            let destPath = joinPaths(tmpDir, fullLocation);

            return symlink(srcPath, destPath);
          })
        );
      })
      .then(() => {
        return tmpDir;
      });
  }

  symlink(fullLocation) {
    this.symlinks.push(fullLocation);
  }

  get tree() {
    let tree = {};

    let writeTree = ([segment0, ...segments], subtree, target) => {
      if (segments.length < 1) {
        subtree[segment0] = target;
        return;
      } if (!subtree[segment0]) {
        subtree[segment0] = {};
      }

      writeTree(segments, subtree[segment0], target);
    };

    this.files.forEach((file) => {
      writeTree(file.relativePath.split('/'), tree, file);
    });

    return tree;
  }

  /**
   * Writes the file set's files.
   * Also creates parent directory, if missing.
   */
  writeToFS({ base = '' }) {
    return Promise.all(
      this.files.map((file) => {
        file.base = base;
        file.writeToFS(this.root);
      })
    );
  }

  /**
   * Returns a file set from an array of absolute paths. Controller is mandatory.
   */
  static fromFS(paths, { base, controller }) {
    return Promise.all(
      paths.map((path) => {
        return File.fromFS(path, { base, root: controller.root })
      })
    )
      .then((files) => {
        return new FileSet({ controller, files });
      });
  }

  static concat(fset0, ...fsets) {
    let fset = fset0.clone();

    return fsets.length > 0
      ? fset.concat(...fsets)
      : fset;
  }
}

/**
 * An object representing a file.
 */
export class File {
  /**
   * Creates a new virtual file from a given path.
   */
  constructor(relativePath, { base = '', contents = '', mode = defaultFileMode } = {}) {
    this._contents = null;

    this.base = base;
    this.contents = contents;
    this.location = dirname(relativePath);
    this.mode = mode;
    this.name = basename(relativePath);
    this.sourceMap = null;
    this.symbol = Symbol();
  }

  /**
   * Tests permissions for the file according to 'mode'.
   */
  access(mode = F_OK) {
    return this.modes.every((localMode) => {
      (localMode & mode) === mode;
    });
  }

  combineSourceMap(originSourceMap) {
    this.sourceMap = File.combineSourceMaps(originSourceMap, this.sourceMap);
  }

  /**
   * Creates a readable stream from the file's content.
   */
  createReadStream() {
    let stream = new ReadableStream();

    stream.push(this.contents);
    stream.push(null);

    return stream;
  }

  /**
   * Returns a cloned file.
   */
  clone() {
    return new File(this.relativePath, {
      base: this.base,
      contents: this.contents,
      mode: this.mode
    });
  }

  /**
   * The file's contents.
   */
  get contents() {
    return this._contents;
  }

  /**
   * Updates the file's contents, forcing it to be a Buffer object.
   */
  set contents(value) {
    this._contents = new Buffer(value);
  }

  /**
   * Generates an absolute path with 'root'.
   */
  generatePath(root) {
    return joinPaths(root, this.base, this.relativePath);
  }

  get modes() {
    return this.mode.toString().split('').map((m) => parseInt(m));
  }

  set modes(modes) {
    this.mode = parseInt(modes.map((m) => m.toString()).join(''));
  }

  /**
   * The file's relative path.
   */
  get relativePath() {
    return joinPaths(this.location, this.name);
  }

  /**
   * Writes the file to 'root'.
   * Also creates parent directory, if missing.
   */
  writeToFS(root) {
    return writeFile(
      this.generatePath(root),
      this.contents,
      { mode: this.mode }
    );
  }

  static combineSourceMaps(...sourceMaps) {
    let [sm0, sm1, ...otherSourceMaps] = sourceMaps;

    let smc0 = new SourceMapConsumer(sm0);
    let smc1 = new SourceMapConsumer(sm1);
    let map = new SourceMapGenerator({ file: smc1.file });

    smc0.eachMapping((mapping) => {
      let pos = smc1.generatedPositionFor({
        source: rawSourceMap0.file,
        line: mapping.generatedLine,
        column: mapping.generatedColumn
      });

      map.addMapping({
        generated: {
          line: pos.line,
          column: pos.column
        },
        source: mapping.source,
        original: {
          line: mapping.originalLine,
          column: mapping.originalColumn
        },
        name: mapping.name
      });
    });

    return otherSourceMaps.length > 0
      ? combineSourceMaps(map, ...otherSourceMaps)
      : map;
  }

  /**
   * Returns a file from its absolute path.
   * Root is mandatory.
   */
  static fromFS(path, { base = '', root }) {
    let relativePath = getPathRelation(joinPaths(root, base), path);

    let stream = createReadStream(path);
    let filePromise = File.fromReadStream(relativePath, stream, { base });

    return Promise.all([
      filePromise,
      stat(path)
    ])
      .then(([file, stats]) => {
        file.mode = stats.mode & parseInt('777', 8);

        return file;
      });
  }

  /**
   * Returns a promise which returns a file once the input stream is ended.
   */
  static fromReadStream(relativePath, inputStream, { base, mode } = {}) {
    let contents = '';
    let deferred = Promise.defer();

    let file = new File(relativePath, { base, mode });
    let stream = new WritableStream();

    stream._write = (chunk, encoding, callback) => {
      contents += chunk.toString();
      callback();
    };

    stream.on('finish', () => {
      file.contents = contents;
      deferred.resolve(file);
    });

    inputStream.on('error', (err) => {
      deferred.reject(err);
    });

    inputStream.pipe(stream);

    return deferred.promise;
  }
}
