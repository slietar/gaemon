/**
 *
 * gaemon
 * CLI
 *
 */


import * as chalk from 'chalk';
import * as yargs from 'yargs';
import { argv } from 'yargs';

import * as api from './cli-api';


yargs
  .usage('Usage: $0 <command> [options] [arguments]')
  .demand(1)
  .command('clean', 'cleans gaemon\'s files (removes temporary files, etc.)', (yargs) => {
    yargs
      .demand(1, 1)
      .option('root', {
        alias: 'r',
        default: process.cwd(),
        describe: 'the path from which everything is done (default to cwd)',
        type: 'string'
      })
  })
  .command('help', 'shows help', (yargs) => {
    yargs.demand(1, 2);
  })
  .command('run', 'runs a task or pipeline', (yargs) => {
    yargs
      .demand(2, 2)
      .option('conf', {
        alias: 'c',
        default: 'auto',
        describe: 'the path to the configuration file (must be relative to root)',
        normalize: true,
        type: 'string'
      })
      .option('debug', {
        alias: 'd',
        default: false,
        describe: 'enables automatic source maps in the configuration file when ES2015 modules are enabled',
        type: 'boolean'
      })
      .option('module-bundler', {
        alias: 'e',
        default: true,
        describe: 'enables ES2015 modules support in the configuration file (with Rollup)',
        type: 'boolean'
      })
      .option('root', {
        alias: 'r',
        default: process.cwd(),
        describe: 'the path from which everything is done (default to cwd)',
        type: 'string'
      })
      .option('watch', {
        alias: 'w',
        default: false,
        describe: 'watches for the input files',
        type: 'boolean'
      });
  });


let command = argv._[0];
let taskName = argv._[1];
let tStart = Date.now();

Promise.resolve()
  .then(() => {
    switch (command) {
      case 'clean':
        return api.clean({
          root: argv.root
        });
      case 'help':
        yargs.showHelp('log');
        return;
      case 'run':
        return api.run(taskName, {
          confFilePath: argv.conf,
          enableModuleBundler: argv['module-bundler'],
          enableSourceMaps: argv.debug,
          root: argv.root,
          watch: argv.watch
        });
      default:
        throw new Error('Unknown command');
    }
  })
  .then((watcher) => {
    if (watcher) {
      let rStart = Date.now();

      watcher
        .on('change', ({ relativePath }) => {
          console.log(`Change detected in ${relativePath}`);
        })
        .on('run', () => {
          console.log(`Running task '${taskName}'`);

          rStart = Date.now();
        })
        .on('run:done', () => {
          let rEnd = Date.now();
          let duration = (rEnd - rStart) / 1000;

          console.log(`Run done in ${duration}s`);
        })
        .on('run:error', (err) => {
          console.error(chalk.yellow(argv.debug ? err.stack : err.message));
        });
    } else {
      let tEnd = Date.now();
      let duration = (tEnd - tStart) / 1000;

      console.log(`Done in ${duration}s`);
    }
  })
  .catch((err) => {
    console.error(chalk.red(argv.debug ? err.stack : err.message));
    process.exit(1);
  });
