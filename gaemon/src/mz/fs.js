/**
 *
 * gaemon
 * modernize filesystem
 *
 */


import * as fs from 'fs-extra';
import { withCallback as thenify } from 'thenify';


export { F_OK, R_OK, W_OK, X_OK } from 'fs-extra';
export { FileReadStream, FileWriteStream, ReadStream, Stats, WriteStream } from 'fs-extra';
export { createOutputStream, createReadStream, createWriteStream } from 'fs-extra';
export { spaces, unwatchFile, watch, watchFile } from 'fs-extra';

export const access = thenify(fs.access);
export const appendFile = thenify(fs.appendFile);
export const chmod = thenify(fs.chmod);
export const chown = thenify(fs.chown);
export const close = thenify(fs.close);
export const copy = thenify(fs.copy);
export const createFile = thenify(fs.createFile);
export const createLink = thenify(fs.createLink);
export const createSymlink = thenify(fs.createSymlink);
export const emptyDir = thenify(fs.emptyDir);
export const emptydir = thenify(fs.emptydir);
export const ensureDir = thenify(fs.ensureDir);
export const ensureFile = thenify(fs.ensureFile);
export const ensureLink = thenify(fs.ensureLink);
export const ensureSymlink = thenify(fs.ensureSymlink);
export const exists = thenify(fs.exists);
export const fchmod = thenify(fs.fchmod);
export const fchown = thenify(fs.fchown);
export const fdatasync = thenify(fs.fdatasync);
export const fstat = thenify(fs.fstat);
export const fsync = thenify(fs.fsync);
export const ftruncate = thenify(fs.ftruncate);
export const futimes = thenify(fs.futimes);
export const gracefulify = thenify(fs.gracefulify);
export const lchmod = thenify(fs.lchmod);
export const lchown = thenify(fs.lchown);
export const link = thenify(fs.link);
export const lstat = thenify(fs.lstat);
export const lutimes = thenify(fs.lutimes);
export const mkdir = thenify(fs.mkdir);
export const mkdirp = thenify(fs.mkdirp);
export const mkdirs = thenify(fs.mkdirs);
export const move = thenify(fs.move);
export const open = thenify(fs.open);
export const outputFile = thenify(fs.outputFile);
export const outputJSON = thenify(fs.outputJSON);
export const outputJson = thenify(fs.outputJson);
export const read = thenify(fs.read);
export const readFile = thenify(fs.readFile);
export const readJSON = thenify(fs.readJSON);
export const readJson = thenify(fs.readJson);
export const readdir = thenify(fs.readdir);
export const readlink = thenify(fs.readlink);
export const realpath = thenify(fs.realpath);
export const remove = thenify(fs.remove);
export const rename = thenify(fs.rename);
export const rmdir = thenify(fs.rmdir);
export const stat = thenify(fs.stat);
export const symlink = thenify(fs.symlink);
export const truncate = thenify(fs.truncate);
export const unlink = thenify(fs.unlink);
export const utimes = thenify(fs.utimes);
export const walk = thenify(fs.walk);
export const write = thenify(fs.write);
export const writeFile = thenify(fs.writeFile);
export const writeJSON = thenify(fs.writeJSON);
export const writeJson = thenify(fs.writeJson);
