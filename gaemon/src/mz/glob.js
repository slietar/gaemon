/**
 *
 * gaemon
 * modernize glob
 *
 */


import * as glob from 'glob';
import { withCallback as thenify } from 'thenify';


const glob0 = thenify(glob);

export default glob0;
export { Glob, hasMagic, sync } from 'glob';
