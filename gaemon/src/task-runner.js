/**
 *
 * gaemon
 * task runner OR iterator agent
 *
 */


import newDebug from 'debug';


const debug = newDebug('gaemon:taskrunner');

/**
 * A task runner.
 * Reserved to internal/complex plugin use.
 */
export default class TaskRunner {
  constructor(generator) {
    this.cache = [];
    this.plugins = [];
    this.generator = [];

    let iterator = generator();

    let iterate = () => {
      let state = iterator.next();
      let plugin = state.value;

      if (state.done) {
        return;
      }

      if (typeof plugin === 'function') {
        plugin = {
          run: plugin,
          runAsPipeline: plugin
        };
      }

      plugin.type = plugin.type || 'transform';
      this.plugins.push(plugin);

      return iterate();
    };

    iterate();

    debug('construct');
  }

  initialize() {
    debug('initialize');

    return Promise.all(
      this.plugins
        .filter((plugin) => plugin.initialize !== void 0)
        .map((plugin, index) => {
          debug('initialize plugin %s initialize', index);
          return plugin.initialize();
        })
    )
      .then(() => {
        debug('initialize done: %s plugins', this.plugins.length);
      });
  }

  run(input) {
    debug('run');

    let subrun = (index = 0) => {
      let plugin = this.plugins[index];

      if (!plugin) {
        debug('run done');
        return Promise.resolve();
      }

      debug('subrun %s', index);

      return Promise.resolve(plugin.run())
        .then(() => {
          debug('subrun %s done', index);

          return subrun(index + 1);
        });
    };

    return subrun();
  }

  runAsPipeline(input) {
    let isFirstRun = this.cache.length < 1;

    debug('run (first run: %s)', isFirstRun);

    let subrun = (input, index = 0) => {
      let plugin = this.plugins[index];

      if (!plugin) {
        debug('run done');
        return input;
      } if (plugin.type === 'unique' && !isFirstRun) {
        debug('subrun %s (skipped)', index);
        return subrun(input, index + 1);
      }

      let inputCached = this.cache[index] || null;

      if (plugin.type === 'bundle') {
        if (inputCached) {
          input = input.concat(inputCached);
        } else {
          debug('subrun %s cache (files: %s)', index, input.files.length);

          this.cache[index] = input;
        }
      }

      debug('subrun %s (plugin type: %s, files: %s, cached: %s)', index, plugin.type, input.files.length, inputCached !== null);

      let deferredOutput = plugin.runAsPipeline !== void 0
        ? plugin.runAsPipeline(input)
        : plugin.run();

      return Promise.resolve(deferredOutput)
        .then((output) => {
          output = output || input.clone();

          debug('subrun %s done', index);

          return subrun(output, index + 1);
        });
    }

    return subrun(input);
  }
}
