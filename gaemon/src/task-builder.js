/**
 *
 * gaemon
 * task builder
 *
 */


export function pipeline(input, output, handler) {
  if (!handler) {
    return new Pipeline({
      handler: output,
      input,
      output: null
    });
  }

  return new Pipeline({
    handler,
    input,
    output
  });
}

export function task(handler) {
  return new Task({
    handler
  });
}


class Task {
  constructor({ handler }) {
    this.handler = handler;
    this.runner = null;
  }

  get isPipeline() {
    return false;
  }
}

class Pipeline extends Task {
  constructor({ handler, input, output }) {
    super({ handler });

    this.input = input;
    this.output = output;
  }

  get isPipeline() {
    return true;
  }
}
