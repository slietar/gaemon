/**
 *
 * gaemon
 * split plugin
 *
 */


import * as Vinyl from '../vinyl';
import TaskRunner from '../task-runner';


export default function split(...generators) {
  return new SplitPlugin(...generators);
}

class SplitPlugin {
  constructor(...generators) {
    this.generators = generators;
  }

  initialize() {
    this.runners = this.generators.map((g) => new TaskRunner());

    return Promise.all(
      this.runners.map((runner, index) => {
        return runner.initialize(this.generators[index]());
      })
    );
  }

  run() {
    return Promise.all(
      this.runners.map((runner) => {
        return runner.run();
      })
    );
  }

  runAsPipeline(input) {
    return Promise.all(
      this.runners.map((runner) => {
        return runner.runAsPipeline(input);
      })
    )
      .then((outputs) => {
        return Vinyl.FileSet.concat(...outputs);
      });
  }
}
