/**
 *
 * gaemon
 * empty plugin
 *
 */


export default function destroy() {
  return function (input) {
    return input.empty();
  };
}
