/**
 *
 * gaemon
 * match plugin
 *
 */


import { join as joinPaths } from 'path';
import minimatch from 'minimatch';


export default function match(pattern, options = {}) {
  return function (input) {
    let output = input.clone();

    output.files = output.files.filter((file) => {
      return (options.inverse || false) !== minimatch(file.relativePath, pattern, options)
    });

    return output;
  };
};
