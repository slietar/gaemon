/**
 *
 * gaemon
 * copy plugin
 *
 */


import { basename, dirname } from 'path';

import * as Vinyl from '../vinyl';
import TaskRunner from '../task-runner';


export default function copy(...args) {
	return new CopyPlugin(...args);
}

class CopyPlugin {
	constructor(originalRelativeFilePath, newRelativeFilePath, generator) {
		this.options = { newRelativeFilePath, originalRelativeFilePath };

		if (generator) {
			this.runner = new TaskRunner(generator);
		}
	}

	initialize() {
		if (this.runner) {
			return this.runner.initialize();
		}
	}

	runAsPipeline(input) {
		let originalRelativeFilePath = this.options.originalRelativeFilePath;

		return Promise.resolve()
			.then(() => {
				let originalFile = input.findFile(originalRelativeFilePath);

		    if (!originalFile) {
		      throw new Error(`Missing file at ${originalRelativeFilePath}`);
		    }

				let file = new Vinyl.File(this.options.newRelativeFilePath, {
					base: originalFile.base,
					contents: originalFile.contents,
					mode: originalFile.mode
				});

				let handlerInput = input.empty({
					files: [file]
				});

				return this.runner
					? this.runner.runAsPipeline(handlerInput)
					: handlerInput;
			})
			.then((output) => {
				return Vinyl.FileSet.concat(output, input);
			});
	}
}
