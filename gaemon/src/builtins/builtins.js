/**
 *
 * gaemon
 * built-in plugins
 *
 **/


import copy from './copy';
import empty from './empty';
import inflate from './inflate';
import log from './log';
import match from './match';
import rename from './rename';
import split from './split';
import tree from './tree';


const Builtins = {
  copy,
  empty,
  inflate,
  log,
  match,
  rename,
  split,
  tree
};

export default Builtins;
