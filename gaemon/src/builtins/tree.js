/**
 *
 * gaemon
 * tree plugin
 *
 **/


export default function tree() {
  return function (input) {
    console.log(['.'].concat(formatTree(input.tree)).join('\n'));
  };
}

function formatTree(tree, bars = []) {
  let lines = [];
  let keys = Object.keys(tree);

  for (let i = 0; i < keys.length; i++) {
    let name = keys[i];
    let value = tree[name];
    let isLast = i >= keys.length - 1;
    let isDirectory = value.constructor === Object;

    lines.push(
      bars.map((bar) => (bar ? '│   ' : '    ')).join('')
    + (isLast ? '└── ' : '├── ')
    + name
    );

    if (isDirectory) {
      lines = lines.concat(
        formatTree(value, bars.concat(!isLast))
      );
    }
  }

  return lines;
}
