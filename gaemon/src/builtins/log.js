/**
 *
 * gaemon
 * log plugin
 *
 */


import newDebug from 'debug';


export default function log(message) {
  return new LogPlugin(message);
}

class LogPlugin {
  constructor(message) {
    this.options = { message };
    this.debug = newDebug('gaemon:plugin:log');

    this.debug('start');
  }

  runAsPipeline(input) {
    this.debug(`${input.files.length} files`);

    if (this.options.message) {
      this.debug(this.options.message);
    }
  };
}
