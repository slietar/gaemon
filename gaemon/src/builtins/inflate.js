/**
 *
 * gaemon
 * inflate plugin
 *
 **/


import * as Vinyl from '../vinyl';


export default function inflate(fileSet) {
  return function (input) {
    if (!(fileSet instanceof Vinyl.FileSet)) {
      fileSet = input.empty({ files: fileSet });
    }

    return input.concat(fileSet);
  };
}
