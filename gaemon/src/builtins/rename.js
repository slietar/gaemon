/**
 *
 * gaemon
 * rename plugin
 *
 */


/**
 * Examples:
 *  - rename('{}.js');
 *  - rename((d) => d.shortname + '.js')
 *  - rename((d) => 'hello.' + d.extensions)
 */
export default function rename(pattern) {
  return function (input) {
    let output = input.clone();

    let handler = typeof pattern === 'function'
      ? pattern
      : (data) => {
        return pattern.replace('{}', data.shortname);
      };

    for (let file of output.files) {
      let segments = file.name.split('.');
      let data = {
        extension: segments.slice(-1)[0],
        extensions: segments.slice(1).join('.'),
        fullname: file.name,
        name: segments.slice(0, -1).join('.'),
        shortname: segments[0]
      };

      file.name = handler(data);
    }

    return output;
  };
}
