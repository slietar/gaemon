/**
 *
 * gaemon
 * controller
 *
 */


import { EventEmitter } from 'events';
import { tmpdir as getTmpDir } from 'os';
import { join as joinPaths } from 'path';
import { rollup } from 'rollup';
import { install as installSourceMaps } from 'source-map-support';
import newDebug from 'debug';

import * as Vinyl from './vinyl';
import { access as accessFile, remove as removeDirectory, unlink as removeFile } from './mz/fs';
import glob from './mz/glob';
import TaskRunner from './task-runner';
import Watcher from './watcher';


const debug = newDebug('gaemon:controller');

const confFileNamePatterns = [
  '.gaemonrc',
  'gaemon.config.js',
  'Gaemonfile',
  'Gaemonfile.js'
];

export default class Controller {
  constructor({ conf = null, root, tasks = {} }) {
    this.conf = conf;
    this.root = root;
    this.tasks = tasks;

    debug('initialize (root: %s)', this.root);

    let osTmpDir = getTmpDir();
    this.tmpDirs = {
      files: joinPaths(osTmpDir, 'gaemon.tmpfiles'),
      filesets: joinPaths(osTmpDir, 'gaemon.tmpfilesets')
    };

    this.tmpFiles = {
      configuration: joinPaths(this.root, '.gaemon.tmpconf')
    };
  }

  /**
   * Deletes temporary directories.
   */
  clean() {
    // small Object.values() polyfill
    function objValues(obj) { return Object.keys(obj).map((prop) => obj[prop]); }

    debug('clean');

    return Promise.all(
      [].concat(
        objValues(this.tmpDirs ).map((path) => removeDirectory(path)),
        objValues(this.tmpFiles).map((path) => removeFile(path).catch(() => {})) // to continue even if the file doesn't exist
      )
    )
      .then(() => {
        debug('clean done');
      });
  }

  findPipeline(pipelineName) {
    debug('find pipeline: %s', pipelineName);

    return this.findTask(pipelineName)
      .then((task) => {
        if (!task.isPipeline) {
          throw new Error(`Task '${pipelineName}' is not a pipeline`);
        }

        return task;
      });
  }

  findTask(taskName) {
    debug('find task: %s', taskName);

    return Promise.resolve()
      .then(() => {
        let task = this.tasks[taskName];

        if (!task) {
          throw new Error(`Unknown task '${taskName}'`);
        }

        return task;
      });
  }

  loadConfiguration({ erase } = {}) {
    if (!this.conf) {
      return Promise.reject(new Error('Missing configuration settings'));
    }

    debug('load configuration (file: %s)', this.conf.relativePath);

    let confFilePath = joinPaths(this.root, this.conf.relativePath);
    let tmpOutputPath = this.tmpFiles.configuration;

    let setTasks = (tasks) => {
      if (erase) {
        this.tasks = tasks;
      } else {
        Object.assign(this.tasks, tasks);
      }

      debug('load configuration done: %s tasks', Object.keys(this.tasks).length);
    };

    if (!this.conf.enableModuleBundler) {
      setTasks(require(confFilePath));

      return Promise.resolve(this.tasks);
    }

    return rollup({
      entry: confFilePath,
      onwarn(message) {
        debug('load configuration rollup warning: ' + message);

        if (/external dependency/.test(message)) {
          return;
        }

        console.log(message);
      }
    })
      .then((bundle) => {
        return bundle.write({
          dest: tmpOutputPath,
          // TODO: Add auto gaemon import with the 'intro' prop in the @controller
          format: 'cjs',
          sourceMap: this.conf.enableSourceMaps ? 'inline' : false
        });
      })
      .then(() => {
        if (this.conf.enableSourceMaps) {
          installSourceMaps();
        }

        setTasks(require(tmpOutputPath));

        return this.tasks;
      });
  }

  runPipeline(pipeline, filePaths) {
    debug('run pipeline');

    return Promise.resolve()
      .then(() => {
        if (!pipeline.runner) {
          pipeline.runner = new TaskRunner(pipeline.handler);
          return pipeline.runner.initialize();
        }
      })
      .then(() => {
        if (filePaths) {
          return filePaths;
        }

        let pattern = joinPaths(pipeline.input, '**/*');

        return glob(pattern, {
          cwd: this.root,
          matchBase: true,
          nodir: true
        });
      })
      .then((paths) => {
        debug('run pipeline glob done: %s files', paths.length);

        paths = paths.map((path) => joinPaths(this.root, path));
        return Vinyl.FileSet.fromFS(paths, {
          base: pipeline.input,
          controller: this
        });
      })
      .then((input) => {
        debug('run pipeline task runner');
        return pipeline.runner.runAsPipeline(input);
      })
      .then((output) => {
        debug('run pipeline done');
        return output.writeToFS({ base: pipeline.output });
      });
  }

  runTask(task) {
    debug('run task');

    if (task.isPipeline) {
      return this.runPipeline(task);
    }

    return Promise.resolve()
      .then(() => {
        if (!task.runner) {
          task.runner = new TaskRunner();
          return task.runner.initialize(task.handler())
        }
      })
      .then(() => {
        debug('run task task runner');
        return task.runner.run();
      })
      .then(() => {
        debug('run task done');
      });
  }

  watchPipeline(pipeline) {
    debug('watch pipeline');

    let watcher = new Watcher(pipeline.input, { root: this.root });

    watcher.on('run:ready', ({ files }) => {
      watcher.run(this.runPipeline(pipeline, files));
    });

    return watcher;
  }

  static findConfiguration({ root }) {
    return new Promise((resolve, reject) => {
      let iterate = (index = 0) => {
        let pattern = confFileNamePatterns[index];

        if (!pattern) {
          reject(new Error(`Configuration file not found at ${this.root}`));
          return;
        }

        let filePath = joinPaths(root, pattern);

        accessFile(filePath)
          .then(() => {
            debug('find configuration done: %s', pattern);
            resolve(pattern);
          })
          .catch((err) => {
            return iterate(index + 1);
          });
      };

      return iterate();
    });
  }
}
