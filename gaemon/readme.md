# Gæmon

Gæmon is a build system simpler than Grunt, Gulp, Brunch.io, Broccoli, and others.
Current at docs revision 14.

[Polytene, polysulfide, polyphenol]


## Features

* CSS, JavaScript & images plugins
* source maps support
* built-in plugins (match, minify, etc.)
* ES2015 (ES6) modules in configuration file
* builds on server
* polyfilling
* built-in watch with incremental builds


## Installation

```sh
# In your project
$ npm install gaemon --save
```

The only requirement is generators; they are available from Node 0.11.x with the `--harmony` flag and enabled from Node 4.x.


## Usage (r14)

Create a new file called `Gaemonfile.js` and write the following code inside:

```js
import babel from 'gaemon-babel';
import httpServer from 'gaemon-http-server';
import typescript from 'gaemon-typescript';

export default gaemon.pipeline('src', 'lib', function* () {
  yield typescript({ target: 'es2015' });
  yield babel({ presets: ['es2015'] });
  yield babel.polyfill();
  yield gaemon.sourceMaps();
});

export const start = gaemon.task(function* () {
  yield this.runPipeline('default');
  yield httpServer({ port: 8000 });
});
```

Then run:
```sh
$ npm install gaemon -g
$ gaemon run start
```


# Documentation

## Summary (r14)

* Use of CommonJS
* Split pipelines/tasks
* Configuration file name
* Server builds
* Build bundle
* Plugin types
* Plugin list
* Plugin development
  * Simple plugins
  * Class plugins
* Source maps
* APIs
  * Configuration
    * Task builder
  * Plugin development
    * Vinyl
    * Task runner
  * Other
    * Controller
    * CLI API
    * Server API
* Watch features
  * Watch specification
  * Incremental builds
  * Plugin types concerning incremental builds
* Speeding up builds
* Development notes
  * Controllers
  * How paths are called
  * How incremental builds work


## Use of commonjs (r14)

If you don't want to use ECMAScript 2015 modules, you can use CommonJS' `require`.
```js
var babel = require('gaemon-babel');

module.exports = gaemon.pipeline(...);
```

Adding the `--no-module-bundling` option (obviously diabling the module bundler) will make tasks and pipelines run faster.


## Split pipelines and tasks (r14)

```js
import { ??? } from 'gaemon';
import minify from 'gaemon-minify';
import sass from 'gaemon-sass';

export default gaemon.pipeline('src', 'lib', function* () {
  yield ???.split(function* () {
    yield ???.match('*.sass');

    yield sass();
  }, function* () {
    yield ???.match('*.css');
  });

  yield ???.runNewTask(function* () {
    ...
  });

  yield minify();
});
```


## Configuration file name (r14)

To make everyone happy, you have four ways of calling your config file:
* Gaemonfile
* Gaemonfile.js
* gaemon.config.js
* .gaemonrc

However, explicitly setting the file path with `--confFilePath` will make Gaemon run faster.


## Builds on server (for later)

As builds can be long and can require power, you can use a server to make them.

On server:
```sh
$ npm install gaemon-server -g
$ gaemon serve --port 3000
```

On client:
```sh
$ gaemon build --server localhost:3000
```

Or you can force it in the config file
```js
gaemon.server('localhost:3000');
```


## Plugins (r12)

There are three types of plugins:
* filter plugins only remove files (destroy, match, etc.)
* spectator plugins do not modify the input file set, (external, JS syntax check, log, runTask, etc.)
* transform plugins remove, modify and add files (babel, chmod, minify, split, etc.)
* write plugins add files (copy, sourceMaps, etc.)

You can access built-in plugins through `gaemon.*`. In tasks, plugins are only spectator-like plugins.

###  Tasks

* normal
  * http server
  * mocha
  * karma

### Pipelines

* filter
  * [built-in] destroy
  * [built-in] match
* spectator
  * [built-in] external
  * [built-in] tree
* transform
  * babel
  * minify
  * postcss
  * rollup
  * sass
* write
  * istanbul
* other
  * [built-in] copy
  * [built-in] runPipeline

### Both

* other
  * [built-in] cleanController
  * [built-in] runTask
  * [built-in] split

### Universal plugins

Universal plugins are plugins that work with CSS as well as JavaScript.

* bundle (browserify in JavaScript, @import resolve in CSS)
* concat
* minify


## Source maps (r14)

```js
import { Vinyl } from 'gaemon';

class Plugin {
  run(input) {
    let output = input.clone();
    let file = new Vinyl.File('hello.js', {
      content: 'Lorem ipsum dolor sit amet',
      sourceMap: { ... }
    });

    output.files.push(file);

    return output;
  }
}

class Plugin {
  run(input) {
    let output = input.empty();

    input.files.forEach((fileInput) => {
      let fileOutput = pluginTransform(fileInput).combineSourceMap(fileInput.sourceMap);

      output.files.push(fileOutput);
    });

    return output;
  }
}
```


## APIs

### Task Builder API

#### gaemon.pipeline(input, output, handler)
Creates a new pipeline from &lt;input> to &lt;output>.


#### gaemon.server(hostname)

### Vinyl API

The vinyl API manages virtual files. It contains two classes: `File` and `FileSet`. It is not recommended to access the `FileSet` constructor directly.

#### new File()
#### File.from()


### Task Runner API (r14)

Let's (re) write the `split` plugin with the Task Runner API.

```js
import { TaskRunner, Vinyl } from 'gaemon';

class SplitPlugin {
  constructor(...generators) {
    this.generators = generators;
  }

  // here becomes useful the `initialize` method
  initialize() {
    this.runners = this.generators.map((generator) => {
      let runner = new TaskRunner();
      runner.initialize(generator());
      return runner;
    });
  }

  run(input) {
    return Promise.all(
      this.runners.map((runner) => {
        return runner.run(input);
      });
    )
      .then((outputs) => {
        return Vinyl.FileSet.concat(...outputs);
      });
  }
}
```

The Task Runner API's main goal is too keep cached files between runs.


## Plugin development (r13)

### Simple plugins

A simple plugin is a function taking an `input` argument which is a file set containing the current files. For tasks, just ignore the `input` argument.
Of course, to allow your plugins to have options, you can create a function that returns a plugin.

```js
function pluginFactory(options) {
  return function plugin(input) {
    ...
  }
}
```

### Class plugins

A class plugin is a class which has at least a constructor, `initialize` and `run` methods.
It is recommended to use a function which returns the built class to remove the `new` keyword in yields.

```js
export function a(options) {
  return new A(options);
}
```

You can still use the following trick.

```js
function A(options) {
  if (!(this instanceof A)) {
    return new A(options);
  }
}
```

#### Transform plugins

Here is an example of a transform-like plugin in ES15.

```js
class TransformPlugin {
  constructor(options) {

  }

  // run once on initialization
  initialize() {

  }

  // run on every build
  run() {

  }
}

// Use of '...args' and not 'options' makes it less dependent on the class constructor
export default function transform(...args) {
  return new TransformPlugin(...args);
}
```

#### Bundle plugins

```js
class BundlePlugin {
  constructor() {

  }

  initialize() {

  }

  // here comes the changing detail
  // you must concatenate missing cached files with the changed input
  run(inputNew, inputCached) {
    // old syntax
    let input = inputNew.clone().merge(inputCached);

    // new low level syntax
    let input = gaemon.vinyl.FileSet.concat(inputNew, inputCached);

    // new high level syntax
    let input = inputNew.cloneConcat(inputCached);

    // new+ syntax
    let input = this.concatCachedWith(inputNew);
  }
}
```

To make this operation automatic, set the `type` property to `bundle`.

```js
class {
  constructor() {
    this.type = 'bundle';
  }
}
```

#### Unique plugins

Unique plugins are plugins that do not have anything in the `run` method.
To save time, just tell Gaemon it is a unique plugin by setting the `type` property to `unique`, or by removing the `run` method.

```js
class {
  constructor() {
    this.type = 'unique';
  }
}
```


## How paths are called (r14)

```
For unity.pipeline('src', ...)
/users/xxxxx/project/ src  /   xxx    /   index.js
        root        | base | location |  (base) name
                    |  full location  |
          base path        |        relative path

Temporary files:
/var/folders/xxx/xxx/xxx/gaemon.tmpdirs/ xxxxx  / ...
         dir root       |  dir prefix  | dir id | ....
                        |      relative dir     | ....
                       dir                      | ...
```


## Controller (r10)

The controller controls everything.

```js
let controller = new gaemon.Controller(process.cwd());

controller.loadConfiguration('Gaemonfile.js', { debug: true })
  .then(() => {
    return controller.runTask('default');
  })
  .then(() => {
    return controller.clean();
  });
```


## Watch features (r12)

### Run iterator with incremental builds

There are two types of plugins when using incremental builds:
* concat: treats all input files together
* transform: treats input files separately

Concat plugins needs to gather all the cached files to treat them, however transform plugins only cares about the given ones.

```
A simple example

         [tr]        [tr]       [tr]        [ct]
input -> match -> typescript -> babel -> browserify -> output
      ^^ changed files                ^^ cached files

A more complex example with the split plugins

          [sp]       [tr]     [tr]       [ct]
input -> split ->|                   |-> minify
                 |-> match -> sass ->|
                 |-> match --------->|
      ^^ changed files                ^^ cached files
                  ^^ changed files
                  ^^ changed files

Another example with file names

a.ts, b.ts -> a.js, b.js   -> bundle.js
a.ts       -> a.js, [b.js] -> bundle.js
```

### Task builder | task runner

```js
var p0 = gaemon.pipeline(...);

var p1 = gaemon.pipeline('a', 'b', function* () {
  yield this.pump(p0);
  yield plugin();
});

function plugin() {
  return function (input, cachedInput) {
    input.merge(cachedInput);
  };
}
```

### Watch system

The watch option implements some particularities:
* waits 500ms before running a pipeline, starting again the time out if another change occurs
* waits for the current run to end before starting a new one caused by a change after these 500ms
* does not wait again when already waiting during a run

When incremental builds are enabled, the 2nd rule starts waiting when it hits a concat-type plugin.
Gaemon also watches the config file.

### Usage with the CLI

Incremental builds are automatically enabled on watch mode; you can disable them with `--no-cache`. Remember not to enable them when using dynamic plugin execution.

### Writing plugins

By default, all plugins are considered as transform plugins, forcing incremental builds.
Here is how to write a concat plugin.

```js
export function foo() {
  return (input, cachedFiles) => {
    var output = input.merge(cachedFiles).clone();

    // ...

    return output;
  };
}
```

### Incremental builds system

```js

// b.scss is modified in the 2nd run

function* () {                       // index | first run input | 2nd run input
  yield split(function* () {         // 0 tr    a.css, b.scss     b.scss
    yield gaemon.match('*.css');     // 1 tr    a.css, b.scss     b.scss
    yield syntaxCheck();             // 2 tr    a.css             ----
  }, function* () {
    yield gaemon.match('*.scss');    // 3 tr    a.css, b.scss     b.scss
    yield compass();                 // 4 tr    b.scss            b.scss
  });

  // resolves @import '...'
  yield bundle();                    // 5 ct    a.css, b.css      a.css, b.scss
  yield minify();                    // 6 tr    bundle.css        bundle.css
  yield rename('bundle.min.css');    // 7 tr    bundle.css        bundle.css
}
```


## Dynamic plugin execution (r12)

This is for rare cases and may cause problems.

```js
gaemon.pipeline('a', 'b', function* () {
  var max = Math.floor(Math.random() * 10);

  for (var i = 0; i < max; i++) {
    yield function (input) {
      console.log('hello world');
    };
  }
});
```


## CLI Options (r12)

```js
export default gaemon.pipeline('a', 'b', function* () {
  yield browserify();

  if (this.options.minify) {
    yield minify();
  }
});
```

Then run:

```sh
$ gaemon run default -- --minify
```
